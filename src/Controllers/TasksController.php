<?php

namespace Jobcerto\Tasks\Controllers;

use App\Http\Controllers\Controller;

class TasksController extends Controller
{
    public function index()
    {
        return view('tasks::index');
    }
}
