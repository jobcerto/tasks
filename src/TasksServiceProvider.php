<?php

namespace Jobcerto\Tasks;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class TasksServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $router->aliasMiddleware('tasks', \Jobcerto\Tasks\Middleware\TasksMiddleware::class);

        $this->publishes([
            __DIR__.'/Config/tasks.php' => config_path('tasks.php'),
        ], 'tasks_config');

        $this->loadRoutesFrom(__DIR__ . '/Routes/web.php');

        $this->loadMigrationsFrom(__DIR__ . '/Migrations');


        $this->loadViewsFrom(__DIR__ . '/Views', 'tasks');

        $this->publishes([
            __DIR__ . '/Views' => resource_path('views/vendor/tasks'),
        ]);

        $this->publishes([
            __DIR__ . '/Assets' => public_path('vendor/tasks'),
        ], 'tasks_assets');

        if ($this->app->runningInConsole()) {
            $this->commands([
                \Jobcerto\Tasks\Commands\TasksCommand::class,
            ]);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/Config/tasks.php', 'tasks'
        );
    }
}
