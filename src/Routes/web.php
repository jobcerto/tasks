<?php

Route::group(['middleware' => 'web', 'namespace' => 'Jobcerto\Tasks\Controllers'], function () {
    Route::middleware('tasks')->group(function () {
        Route::resource('tasks', 'TasksController');
    });
});
